#!/usr/bin/python3
# This is a program to plan meals and shopping for the week

import random
import yaml
import os
import copy

working_dir = "/home/davide/projects/meal-planner/dishes"

dishes = []     # The list of all dishes
mains = []      # The list of all main dishes
sides = []      # The list of all side dishes
firsts = []     # The list of all first dishes

week = ['monday','tuesday','wednesday','thursday','friday','saturday','sunday']
plan = {}

for day in week:
    plan[day] = {}

min_carbs = 100
min_fats = 50
min_proteins = 50

max_carbs = 1000
max_fats = 500
max_proteins = 400

shopping_list = [] 
shopping_list_crude = []
shopping_list_doubled = []
shopping_list_final = []

# Importing dishes
print("")
print("Dishes found:")
print("")

for file in os.listdir(working_dir):
    print("- " + file)
    with open(working_dir + '/' + file, 'r') as dish:
        dishes.append(yaml.safe_load(dish))

print("")
    
# Sorting the dishes list into first, main and side dishes

for i in dishes:
    if i['flags']['is_main'] == "y":
        mains.append(i)
    if i['flags']['is_side'] == "y":
        sides.append(i)
    else:
        firsts.append(i) 

# choosing random combination for the day, for every day of the week and
# compose a crude shopping list

for day in week:
    # Choose randomly 1 first, 1 main and 1 side that respect a macronutrient condition
    if day == ('monday' or 'wednesday' or 'friday' or 'sunday'):
        while True:
            r_first = random.choice(firsts)
            r_main = random.choice(mains)
            r_side = random.choice(sides)

            total_carbs     = r_first['macro']['carbs'] + r_main['macro']['carbs'] + r_side['macro']['carbs']
            total_fats      = r_first['macro']['fats'] + r_main['macro']['fats'] + r_side['macro']['fats']
            total_proteins  = r_first['macro']['proteins'] + r_main['macro']['proteins'] + r_side['macro']['proteins']

            if (total_carbs     in range(min_carbs, max_carbs)  and 
                total_fats      in range(min_fats, max_fats)    and
                total_proteins  in range(min_proteins, max_proteins)):
                    break
    else:
        # don't re-roll side dishes
        while True:
            r_first = random.choice(firsts)
            r_main = random.choice(mains)

            total_carbs     = r_first['macro']['carbs'] + r_main['macro']['carbs'] + r_side['macro']['carbs']
            total_fats      = r_first['macro']['fats'] + r_main['macro']['fats'] + r_side['macro']['fats']
            total_proteins  = r_first['macro']['proteins'] + r_main['macro']['proteins'] + r_side['macro']['proteins']

            if (total_carbs     in range(min_carbs, max_carbs)  and 
                total_fats      in range(min_fats, max_fats)    and
                total_proteins  in range(min_proteins, max_proteins)):
                    break

    plan[day]['lunch'] = r_first
    plan[day]['dinner'] = r_main
    plan[day]['side'] = r_side

    shopping_list_crude = shopping_list_crude + r_first['ingredients'] + r_main['ingredients'] + r_side['ingredients']

# Rationalize the crude shopping list
def distinguish(crude):
    final = []
    doubled = []
    for item_c in crude:

        collision = False
    
        if not final:
            final.append(item_c)
        else:
            for item_f in final:
                if item_c['item'] == item_f['item']:
                    collision = True

            if collision == True:
                doubled.append(item_c)
            else:
                final.append(item_c)
    return final, doubled

shopping_list, shopping_list_doubled = distinguish(shopping_list_crude)


# ABSOLUTELY FUNDAMENTAL PASSAGE
# If deepcopy is not done it's just a reference: values of
# shopping_list_crude and shopping_list_final gets updated too
# by sum_doubled()
shopping = copy.deepcopy(shopping_list)
shopping_doubled = copy.deepcopy(shopping_list_doubled)


def sum_doubled(final, doubled):
    for item_final in final:
        for item_doubled in doubled:
            f = item_final['quantity'] 
            d = item_doubled['quantity']
            if item_doubled['item'] == item_final['item']:
                quantity = f + d
                item_final['quantity'] = quantity
    return final

list_final = sum_doubled(shopping, shopping_doubled)
shopping_list_final = copy.deepcopy(list_final)

print('Weekly plan: ')
print('')
for day in week:
    print(day + ':')
    print('     lunch: ' + plan[day]['lunch']['name'])
    print('     dinner: ' + plan[day]['dinner']['name'])
    print('     side: ' + plan[day]['side']['name'])
    print('')

print('')
print('Shopping list:')
for item in shopping_list_final:
    print('  - ' + item['item'] + ' x ' + str(item['quantity']) + ' ' + item['measure'])


# tasks:
# - remove dish from the list after they are chosen (not the same dish twice in a week)
# - split the shopping list into 2 (i.e. monday and thursday/friday)
# - export plan and shopping lists as calendar events
