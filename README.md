# Meal-planner

## Description

Script that reads from a list of recipes and compose a weekly menu with all the necessary ingredients.

## Libraries used

- random
- os
- copy
- yaml
